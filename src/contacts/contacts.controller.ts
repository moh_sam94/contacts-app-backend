import { ContactService } from '../contact/contact.service';
import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
} from '@nestjs/common';
import { Contact } from '../entities/contact.entity';
import { Response } from 'express';
import { Res } from '@nestjs/common';

@Controller('contacts')
export class ContactsController {
  constructor(private contactService: ContactService) {}

  @Get()
  read(): Promise<Contact[]> {
    return this.contactService.readAll();
  }

  @Get(':id')
  async readOne(@Param('id') id): Promise<Contact> {
    return this.contactService.read(id);
  }

  @Post('create')
  async create(@Body() contact: Contact): Promise<any> {
    return this.contactService.create(contact);
  }

  @Put(':id/update')
  async update(@Param('id') id, @Body() contact: Contact): Promise<Contact> {
    contact.id = Number(id);
    this.contactService.update(contact);
    return this.contactService.read(id);
  }

  @Delete(':id/delete')
  async delete(@Param('id') id, @Res() res: Response): Promise<any> {
    const contact = await this.contactService.read(id);
    this.contactService.delete(id);
    return res.json({ ...contact });
  }
}
